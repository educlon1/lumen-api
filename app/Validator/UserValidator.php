<?php

namespace App\Validator;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserValidator
{

    /**
     * @var Request
     */
    public $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function validate()
    {
        return Validator::make($this->request->all(), $this->rules(), $this->messages());
    }

    private function rules()
    {
        return [
            "firstname" => "required",
            "lastname" => "required",
            "document_type" => "required",
            "document_number" => "required|unique:users,document_number," . $this->request->id,
            "email" => "required|email|unique:users,email," . $this->request->id,
            "password" => "required",
            "confirm_password" => "required|same:password",
            "phone" => "required"
        ];
    }

    private function messages()
    {
        return [
            "firstname.required" => "El NOMBRE es obligatorio",
            "lastname.required" => "El APELLIDO es obligatorio",
            "document_type.required" => "El TIPO DE DOCUMENTO es obligatorio",
            "document_number.required" => "El NUMERO DE DOCUMENTO es obligatorio",
            "document_number.unique" => "El NUMERO DE DOCUMENTO debe ser único",
            "email.required" => "El EMAIL es obligatorio",
            "email.unique" => "Este EMAIL ya está en uso",
            "password.required" => "El PASSWORD es obligatorio",
            "confirm_password.required" => "El CONFIRM PASSWORD es obligatorio",
            "confirm_password.same" => "Los PASSWORDS no coinciden",
            "phone.required" => "El TELÉFONO es obligatorio"
        ];
    }
}


