<?php

namespace App\Services\Implementation;

use App\Services\Interfaces\IUserServiceInterface;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserServiceImpl implements IUserServiceInterface 
{

    private $model;

    function __construct()
    {
        $this->model = new User();
    }

    function getUser()
    {
        return $this->model->withTrashed()->get();
    }


    function getUserById(int $id)
    {

    }

    /**
     * Crea un Usuyario en la BD
     */
    function postUser(array $user)
    {
        $user['password'] = Hash::make($user['password']);
        $this->model->create($user);
    }


    function putUser(array $user, int $id)
    {
        $user['password'] = Hash::make($user['password']);
        $this->model->where('id', $id)
                    ->first()
                    ->fill($user)
                    ->save();
    }


    function delUser(int $id)
    {
        $user = $this->model->find($id);

        if($user != null){
            $user->delete();
        }
    }


    function restoreUser(int $id)
    {
        $user = $this->model->withTrashed()->find($id);

        if($user != null){
            $user->restore();
        }
    }


}